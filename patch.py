import requests
import json

url = "https://192.168.146.251/api/dcim/sites/"

payload = json.dumps([
   {
      "id": 22,
      "name": "takoradi",
      "slug": "takoradi-gh",
      "status": "planned"
   }
])
headers = {
   'Authorization': 'Token  9c79d328d4bf27466d709a7d0776b5fdb3ab80a2',
   'User-Agent': 'Apidog/1.0.0 (https://apidog.com)',
   'Content-Type': 'application/json'
}

response = requests.request("PATCH", url, headers=headers, data=payload, verify=False)

print(response.text)

# Convert the response text to a Python dictionary and then pretty-print it
response_json = json.loads(response.text)
pretty_json = json.dumps(response_json, indent=4)
print(type(pretty_json))
print(response_json[0]['url'])