# Adjusting the script to include reading from the sites_data.json file
import requests
import json
from time import sleep

def create_sites_from_file(json_file_path):
    try:
        with open(json_file_path, 'r') as file:
            sites_data = json.load(file)
    except Exception as e:
        print(f"Failed to read {json_file_path}: {e}")
        return

    url = "https://192.168.146.251/api/dcim/sites/"
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Token 9c79d328d4bf27466d709a7d0776b5fdb3ab80a2',
        'Cookie': 'csrftoken=VVWcJrE4yFr9opXzB1Y6RJOiOOVS7jEM'
    }

    for site in sites_data:
        payload = json.dumps({
            "name": site['name_site'],  # Adjusted to match key in JSON
            "slug": site['slug'],
            "status": site['status']
        })
        
        attempt = 0
        success = False
        while attempt < 3 and not success:
            try:
                response = requests.post(url, headers=headers, data=payload, verify=False)
                # response = requests.post(url, headers=headers, data=payload)
                if response.status_code == 201:
                    print(f"✅ Successfully created site {site['name_site']}.\n\n")
                    success = True
                else:
                    print(f"❌ Failed to create site {site['name_site']}: {response.status_code}, {response.text}\n\n")
                    if response.status_code in [429, 503]:
                        sleep((2 ** attempt) * 1)
                    else:
                        break
            except requests.exceptions.RequestException as e:
                print(f"Request failed for site {site['name_site']}: {e}")
            attempt += 1

# This version of the function includes reading from the specified JSON file path, loading the sites data,
# and then processing each site as before with enhanced error handling and retry logic.

# To execute this script, you would call create_sites_from_file('/path/to/sites_data.json'),
# replacing '/path/to/sites_data.json' with the actual path to your JSON file.


if __name__ == "__main__":
    # Replace '/path/to/sites_data.json' with the actual path to your JSON file
    json_file_path = '/home/gitlab-runner/builds/EpPhHSFk/0/kelvinwest32/postman/sites_data.json'
    create_sites_from_file(json_file_path)
